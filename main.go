package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/dk-group-proyects/nghfs/src/app"
	"gitlab.com/dk-group-proyects/nghfs/src/serverError"
	"golang.ngrok.com/ngrok"
	"golang.ngrok.com/ngrok/config"
	"mjpclab.dev/ghfs/src/param"
	"mjpclab.dev/ghfs/src/setting"
)

// func Start(token string, ctx context.Context, args []string, c chan string) {
// 	//src.Start(token, ctx, args, c)
// }

func Start(token string, ctx context.Context, args []string, c chan string) {
	//params, _, _, errs := param.ParseFromArgs(args)
	params, _, _, errs := param.ParseFromCli()
	serverError.CheckFatal(errs...)
	fmt.Println(&params)

	// setting
	setting := setting.ParseFromEnv()

	// handler
	ghfs, _ := app.NewHandler(params, setting)
	if ghfs == nil {
		serverError.CheckFatal(errors.New("failed to create application instance"))
	}
	//use from external ctx like
	//ctx := context.Background()
	l, err := ngrok.Listen(ctx,
		config.HTTPEndpoint(
		//config.WithDomain("d3stroy3r-app-ngrok.io"),
		),

		//ngrok.WithAuthtokenFromEnv(),
		ngrok.WithAuthtoken(token),
	)
	if err != nil {
		return
	}
	//c <- fmt.Sprintln("ngrok ingress url: ", l.Addr())
	c <- l.Addr().String()
	http.Serve(l, ghfs)

}
func main() {
	token := os.Getenv("NGROKTOKEN")
	c := make(chan string)
	ctx := context.Background()
	var listargs []string

	go func() {
		dir, _ := os.Getwd()
		listargs = append(listargs, dir)
		Start(token, ctx, listargs, c)
	}()
	cadenea := <-c
	close(c)
	fmt.Println("En main", cadenea)
}
