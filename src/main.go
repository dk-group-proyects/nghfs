package src

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/dk-group-proyects/nghfs/src/app"

	"mjpclab.dev/ghfs/src/serverError"
)

func cleanupOnEnd(appInst *app.App) {
	chSignal := make(chan os.Signal)
	signal.Notify(chSignal, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-chSignal
		appInst.Shutdown()
		os.Exit(0)
	}()
}

func reopenLogOnHup(appInst *app.App) {
	chSignal := make(chan os.Signal)
	signal.Notify(chSignal, syscall.SIGHUP)

	go func() {
		for range chSignal {
			errs := appInst.ReOpenLog()
			serverError.CheckFatal(errs...)
		}
	}()
}

// func Main() {
// 	// params
// 	params, _, _, errs := param.ParseFromCli()
// 	serverError.CheckFatal(errs...)

// 	// setting
// 	setting := setting.ParseFromEnv()

// 	// app
// 	appInst, errs := app.NewApp(params, setting)
// 	serverError.CheckFatal(errs...)
// 	if appInst == nil {
// 		// fallo al desplegar la app
// 		serverError.CheckFatal(errors.New("failed to create application instance"))
// 	}

// 	cleanupOnEnd(appInst)
// 	reopenLogOnHup(appInst)
// 	errs = appInst.Open()
// 	serverError.CheckFatal(errs...)
// 	appInst.Shutdown()
// }
